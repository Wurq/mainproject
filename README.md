m68项目之生产环境部署方案
---
# 需求：
  * 项目需要在安全域内进行快速开发，部署生产环境，实现一系列的自动化操作。
    * 需要对项目的服务端功能进行自动化测试，并生成二进制压缩包版本的服务端。
    * 需要对项目的客户端版本进行自动化打包，快速生成对应的客户端版本。
    * 需要对生成的服务端(压缩包版本)和客户端(android/ios)从域内传输到域外。
  * 生产环境包含：
    * 配置CI/CD(持续集成/持续交付)方案，实现自动化测试。
    * 配置快速打包(存在不同版本)方案，实现自动化打包。
    * 配置安全传输(从域内传到域外)方案，实现自动化传输。
    ```mermaid
        graph LR
            Env_CICD[配置CI/CD方案-持续集成/持续交付]
            Env_PACK[配置快速打包方案-存在不同版本]
            Env_TRANSMIT[配置安全传输方案-从域内传到域外]
        subgraph 生产环境 
            Env_CICD --> 实现自动化测试
            Env_PACK --> 实现自动化打包
            Env_TRANSMIT --> 实现自动化传输
        end
    ```
  * 防止项目的源代码泄露，进行严格的权限管控。
    * **在生产环境中，需要将测试阶段和打包/传输阶段隔离。**
        * 开发人员可以编写自动化测试的相关脚本，存在安全隐患，具体原因在配置CI/CD方案中描述。
        * 需要将打包/传输阶段与开发人员进行隔离，严格管控代码审核。
        ```mermaid
            graph LR
                Developer[开发人员]
                Env_CICD[测试阶段]
                Env_PACK[打包阶段]
                Env_TRANSMIT[传输阶段]

                Developer -- 编写自动化测试脚本 --> Env_CICD
                Env_CICD -- 测试成功后,且为tag分支 --> Env_PACK
                Env_PACK -- 打包成功后 -->  Env_TRANSMIT

                subgraph 提供测试的服务器 
                    Developer
                    Env_CICD
                end
                subgraph 提供打包的服务器
                    Env_PACK
                end
                subgraph 提供传输的服务器
                    Env_TRANSMIT
                end
        ```
---
# 生产环境：
## 配置CI/CD方案-持续集成/持续交付：
* 需求：
    * 需要对项目的服务端功能进行自动化测试，并生成二进制压缩包版本的服务端。
* 概述：
    * CI/CD是指持续集成/持续部署。
        * 持续集成(Continuous integration)是一种软件开发实践，每次集成都通过自动化的构建（包括编译，发布，自动化测试）来验证，从而尽早地发现集成错误。
        * 持续部署（continuous deployment）是通过自动化的构建、测试和部署循环来快速交付高质量的产品。
    * 中心思想是当每一次push到gitlab的时候，都会触发一次脚本执行，然后脚本的内容包括了测试，编译，部署等一系列自定义的内容。
* CI/CD的好处：
    * 快速发现错误。
        * 每完成一点更新，就集成到主干上，可以快速发现错误，定位错误也比较容易。
    * 防止分支大幅度偏离主干。
        * 如果不是经常集成，主干又在不断更新，会导致以后集成的难度变大，甚至难以集成。
* 通过Git实现项目的版本控制。
    * Git是目前世界上最先进的分布式版本控制系统。
    * master为主分支，每个developer可以创建自己的分支，并在该分支上进行开发。
    * 每个分支均是项目的完整版本。
* 通过Gitlab维护存放项目的Git库。
    * 通过Gitlab维护master主分支，方便developer根据需求创建/合并分支到master中。
    * 提出需求：
        * 通过Gitlab的Issues创建对应的详细需求。
    * 领取需求，创建分支：
        * 通过点击对应Issue的Create merge request按钮, 可创建该Issue对应的分支。
        * **该分支是以最新的master为基础创建新的分支，防止分支大幅度偏离主干。**
    * 完成需求，测试代码：
        * **在每次发生创建新的分支和提交分支改动时，均会执行一系列测试操作，能够快速发现错误。**
    * 代码审核：
        * 每个分支的改动均可以在Gitlab上直接查看，评论和提出代码问题，方便代码审核和开发交流。
    * 合并分支：
        * 如果有配置Gitlab-CI/CD时，则需要在所有案例均测试成功才能进行Merge操作。
        ```mermaid
            graph TD
                QUESTER[需求人员]
                DEVELOPER[开发人员]
                APPLY_ISSUE[新的需求]
                ISSUE_LIST[任务列表]
                GET_ISSUE[领取需求,创建分支]
                COMMIT_CODE[提交分支改动]
                TEST_CODE[代码测试]
                REVIEW_CODE[代码审核]
                MERGE_BRANCH[合并分支到master]

                QUESTER -- 提出 --> APPLY_ISSUE
                APPLY_ISSUE -- 添加 --> ISSUE_LIST

                ISSUE_LIST -- 分配任务 --> DEVELOPER
                DEVELOPER -- 领取任务 --> GET_ISSUE
                DEVELOPER -- 解决任务 --> COMMIT_CODE
                COMMIT_CODE -- 更新远程分支 --> TEST_CODE
                TEST_CODE -- 测试通过 --> REVIEW_CODE 
                REVIEW_CODE -- 审核通过 --> MERGE_BRANCH

            subgraph 
                QUESTER
                APPLY_ISSUE
                ISSUE_LIST
            end
            subgraph 
                ISSUE_LIST
                DEVELOPER
                GET_ISSUE
                COMMIT_CODE
                TEST_CODE
                REVIEW_CODE 
                MERGE_BRANCH
            end
        ```
    * Gitlab-CI/CD中的基本概念:
      * Pipeline:
          * Pipeline表示构建任务，包含测试，编译，部署等一系列的阶段的执行。
          * 每次提交都会创建一条Pipeline。
      * Stages:
          * Stages表示构建阶段，属于Pipeline的组成单元。
          * Stages的特点为：
              * 当前Stages都执行成功，才会执行下一个Stages。
              * 所有的Stages都执行成功， 该pipeline才会成功。
          * 一般将Stages分为三个阶段：build，test，deploy。
      * Jobs:
          * Jobs表示构建工作，属于Stages的组成单元。
          * Jobs的特点为：
              * 每个Stages中的所有Job执行成功，该Stages才会执行成功。
              * 默认情况下，每个Job都是单独的环境，每个job直接不互相干扰。
                  * 如果存在job之间相互依赖，则可以通过artifacts或cache实现。
      * Gitlab-CI：
          * Gitlab自带的持续集成系统，.gitlab.yml的脚本解析就由它来负责。
      * Gitlab-Runner：
          * 负责.gitlab-ci.yml脚本的部分执行，.gitlab-ci.yml的script部分的运行就是由runner来负责的。
          * Gitlab-CI通过解析.gitlab.yml文件后，根据里面的规则，分配对应的runner执行相应脚本script。
              * 每个runner都有自己的一个tag，作为向Gitlab注册的标签，Gitlab-CI可根据该tag执行指定runner。
          * 一台服务器/pc可以注册多个Runner，要让Runner同时运行，则需要进一步参考官方文档。
          * 注册runner直接参考gitlab-runner官方文档即可：https://docs.gitlab.com/runner/
      * .gitlab.yml：
          * 必须存在与git项目的根目录下的一个文件, 记录了一系列的执行阶段和执行规则。
          * 具体实现通过.gitlab.yml实现：https://docs.gitlab.com/ce/ci/yaml/README.html
      * **安全隐患**：
          * 由于Gitlab-Runner是负责解析执行.gitlab.yml脚本，Developer需要编写.gitlab-ci.yml添加新的测试流程。
          * Developer可以通过Gitlab-Runner获取其所在服务器的管理员权限，从而执行一些不可管控的操作。
          * 因此，需要将提供测试的服务器和提供打包/传输的服务器隔离开，确保文件传输是可控，可追溯的。
      * Pipeline流程图:
          ```mermaid
              graph LR
              Job_build1[执行服务端测试-源代码版本-windows]
              Job_build2[执行服务端测试-源代码版本-linux]
              Job_pack[生成服务端的BZP版本,BINARY,ZIP,PAK]
              Job_test1[执行服务端测试-BZP版本-windows]
              Job_test2[执行服务端测试-BZP版本-linux]
              Job_deploy[执行部署操作]
              check1{是否全部成功}
              check_pack{生成BZP版本}
              check2{是否全部成功,且为tag分支}
              Job_build1 --> check1
              Job_build2 --> check1
              check1 --全部通过--> Job_pack
              check1 --未全部通过--> End
              Job_pack --> check_pack
              Job_pack --> check_pack
              check_pack --全部通过--> Job_test1
              check_pack --全部通过--> Job_test2
              check_pack --未全部通过--> End
              Job_test1 --> check2
              Job_test2 --> check2
              check2 --全部通过--> Job_deploy
              check2 --未全部通过--> End
              subgraph 执行测试-源代码版本
              Job_build1
              Job_build2
              end
              subgraph 生成打包版本
              Job_pack
              end
              subgraph 执行测试-BZP版本
              Job_test1
              Job_test2
              end
              subgraph 执行自动化部署
              Job_deploy
              end
          ```
---
## 配置快速打包方案-存在不同版本：
* 需求：
    * 需要对项目的客户端版本进行自动化打包，快速生成对应的客户端版本。
    * 出于对Gitlab-CI/CD的安全隐患的考虑，需要隔离提供测试的服务器和提供打包的服务器。
* 如何隔离服务器：
    * 打包服务器不能直接通过Gitlab-CI/CD执行脚本。
    * 在打包服务器上实现监控程序，用于实时读取指定地址的配置文件，并根据配置文件执行打包操作。
        * 配置文件由Gitlab-CI/CD的最后部署阶段在指定目录生成。
        * **配置文件记录了项目的git库的tag名，根据tag名切换至指定版本**。
        * 根据打包服务器上预设的打包流程的脚本，执行打包操作，确保打包阶段不依赖项目文件。
    ```mermaid
        graph LR
            ENV_CICD[Gitlab-CI/CD最终部署阶段]
            MONITOR_EXE[打包服务器上的监控程序]
            PACK_EXE[打包服务器上的打包操作]
            FILE_CONFIG[配置文件]
            ENV_TRANSIT[传输服务器上的指定目录]

            ENV_CICD -- 在指定地址中生成配置文件 --> FILE_CONFIG
            
            FILE_CONFIG -- 提供配置文件 --> MONITOR_EXE
            MONITOR_EXE -- 根据配置信息,执行打包操作 --> PACK_EXE
            PACK_EXE -- 拷贝打包文件至 --> ENV_TRANSIT
        subgraph 提供测试的服务器
            ENV_CICD
        end
        subgraph 存放配置文件的指定地址
            FILE_CONFIG
        end
        subgraph 提供打包的服务器
            MONITOR_EXE
            PACK_EXE
        end
        subgraph 提供传输的服务器
            ENV_TRANSIT
        end

    ```
* 如何区分不同版本进行自动化打包：
    * 客户端的打包操作有两种版本，两个类型。
        * 两种版本：android，ios。
        * 两种类型：完整包，补丁包。
    * 根据Gitlab创建的tag名称，进行区分不同版本和不同类型。
        * 完整包：**创建前缀为'tag_full_'的tag**。
            * tag_full_ios_version: 仅打包ios的完整包。
            * tag_full_android_version: 仅打包android的完整包。
            * tag_full_version: 未指定版本，默认为打包两种版本(ios,android)的完整包。
                * 打包两种版本的完整包的tag一般取名为tag_full_all_version。
        * 补丁包：**创建前缀'tag_patch_'的tag**。
            * tag_patch_ios_version: 仅打包ios的补丁包。
            * tag_patch_andorid_version: 仅打包android的补丁包。
* 如何执行快速打包：
    * 随着项目的推进，在客户端的打包流程中Unity在切换平台时(android/ios)会有较多时间开销。
    * 在不切换平台的情况下能够极大缩短打包流程的时间，以下提供两种方案。
        * 配置两台打包服务器，分别用于打包android和ios版本的客户端，能够并行执行打包操作。
        * 在Unity切换不同平台时，会重新生成Library文件夹，保留不同平台的Library文件夹，例：
            * 将android平台的Library更改名字为Library_android。
            * 在切换为android平台时，将该Library_android修改为Library即可。

* 自动打包环境的流程图：
    ```mermaid
      graph LR
        Developer -- 通过Gitlab创建完整包/补丁包的tag --> Gitlab-CI
        Gitlab-CI -- 解析.gitlab.yml执行命令行指令 --> 执行打包操作
    ```
    ```mermaid
      graph TD
        pack[执行打包操作]
        pack_full[执行打完整包操作]
        pack_patch[执行打补丁包操作]
        packing_full[执行指定版本的打完整包操作]
        packing_patch[执行指定版本的打补丁包操作]
        packing_full_all[执行所有版本android,ios,win的打完整包操作]
        packing_patch_all[执行所有版本android,ios的打补丁包操作]
        transform_package[传输压缩包文件至指定目录]
        check_tag_pre{识别tag名称}
        check_tag_full{tag名称是包含android/ios/win}
        check_tag_patch{tag名称是包含android/ios}

        pack --> check_tag_pre
        check_tag_pre -- tag名称前缀为tag_full_ --> pack_full
        check_tag_pre -- tag名称前缀为tag_patch_ --> pack_patch

        pack_full --> check_tag_full
        pack_patch --> check_tag_patch

        check_tag_full -- 是 --> packing_full
        check_tag_full -- 否 --> packing_full_all
        check_tag_patch -- 是 --> packing_patch
        check_tag_patch -- 否 --> packing_patch_all

        packing_full --> transform_package
        packing_patch --> transform_package
        packing_full_all --> transform_package
        packing_patch_all --> transform_package
    ```
---
## 配置安全传输方案-从域内传到域外：
* 需求：
    * 需要对生成的服务端(压缩包版本)和客户端(android/ios)从域内到域外。
* 如何进行安全传输-从域内传到域外:
    * 域内文件传输方案：
        * 传输服务器是从域内传输文件到域外的出口，与该机器有交互的其他机器也都要严格管控。
        * 打包服务器只能读取指定地址的配置文件和拷贝文件至传输文件的指定目录。
        * 传输服务器只提供共享目录给打包服务器进行文件拷贝，且没有运行权限。
    * 域外文件传输方案：
        * 传输服务器先将文件上传至FTP服务器(域内)，然后向中转服务器发送发布请求，并进行验证。
        * 验证成功后，域外中转服务器从FTP服务器(域内)拉取指定文件，并将该文件发送至目标服务器(域外)。
    ```mermaid
      graph TD
        SHARE_DIR[存放配置文件的服务器]
        ENV_PACK[打包服务器]
        ENV_TRANSIT[传输服务器]
        ENV_FTP[FTP服务器]
        ENV_SWITCH_1[中转服务器-域内] 
        ENV_SWITCH_2[中转服务器-域外] 
        ENV_TARGET[目标服务器]

        SHARE_DIR -- 获取配置文件的信息 --> ENV_PACK
        ENV_PACK -- 生成指定版本的客户端和压缩包版的服务端 --> ENV_TRANSIT
        ENV_TRANSIT -- 将传输文件上传至文件服务器 --> ENV_FTP
        ENV_TRANSIT -- 发送发布请求 --> ENV_SWITCH_1
        ENV_SWITCH_1 -- 发送验证消息 -->  ENV_FTP
        ENV_FTP -- 验证成功后,拉取指定文件 --> ENV_SWITCH_1
        ENV_SWITCH_1 -- 传输至域外服务器 --> ENV_SWITCH_2
        ENV_SWITCH_2 -- 拉取成功后,拷贝至目标服务器 --> ENV_TARGET

        subgraph 域内环境
            SHARE_DIR
            ENV_PACK
            ENV_TRANSIT
            ENV_FTP
        end
        subgraph 中转服务器
            ENV_SWITCH_1
            ENV_SWITCH_2
        end
        subgraph 域外环境
            ENV_TARGET
        end

    ```